var marker

function initMap() {
	var latlng = new google.maps.LatLng(
		window.initLat || 49.988358,
		window.initLng || 36.232845
	)

	var myOptions = {
		zoom: 16,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		streetViewControl: false,
		mapTypeControl: false,
	}

	var map = new google.maps.Map(document.getElementById('map'), myOptions)

	function placeMarker(location) {
		if (marker == undefined) {
			marker = new google.maps.Marker({
				position: location,
				map: map,
				animation: google.maps.Animation.DROP,
			})
		} else {
			marker.setPosition(location)
		}
		map.setCenter(location)
	}

	placeMarker(latlng)

	var a = document.querySelectorAll('.js-map-position')

	a.forEach(function (div) {
		div.addEventListener('click', function (event) {
			var contactBlock = event.target.closest('.js-map-position')

			if (contactBlock) {
				var lng = contactBlock.getAttribute('data-lng')
				var lat = contactBlock.getAttribute('data-lat')

				var position = new google.maps.LatLng(lat, lng)
				placeMarker(position)
			}
		})
	})
}
