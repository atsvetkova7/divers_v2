$(function () {
	$(document).ready(function () {
    var $modalCItyDropdown = $("#modal-city--dropdown");
    var $confirmBtn = $("#js-confirm-city");

		function initOpenCityModal(element) {
			if (element && element.length) {
				$.magnificPopup.open({
					items: {
						src: element,
					},
					type: "inline",
					fixedContentPos: false,
					fixedBgPos: false,
					closeMarkup: '<button class="mfp-close "></button>',
					callbacks: {
						beforeOpen: function () {
							$("body").addClass("open-modal");
							element.css({ display: "block" });
						},
						afterClose: function () {
							$("body").removeClass("open-modal");
						},
					},
				});
			}
		}

		$confirmBtn.on("click", function () {
			$.magnificPopup.close();
		});

		initOpenCityModal($modalCItyDropdown);
	});
});
