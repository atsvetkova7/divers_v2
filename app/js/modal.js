$(function () {
	$(document).ready(function () {
		function openGiftCourse() {
			var $signUpButton = $('.js-open-gift-course')
			var $signUpCourseModal = $('#gift-course-popup')

			function handlerOpenPopup() {
				var button = $(this)

				$.magnificPopup.open({
					items: {
						src: '#gift-course-popup',
						type: 'inline',
					},
					fixedContentPos: false,
					fixedBgPos: false,
					closeMarkup: '<button class="mfp-close "></button>',
					callbacks: {
						beforeOpen: function () {
							$('body').addClass('open-modal')
							var courseContent = button.closest('.js-course-details')
							var courseTitle = courseContent.attr('data-course-title')
							$('#gift-course-popup .course-name').val(courseTitle)
							var courseRadioButton = courseContent.find('input:checked')
							if (courseRadioButton) {
								var coursePrice = courseRadioButton.val()
								$('#gift-course-popup .course-price').val(coursePrice)
							}
							$signUpCourseModal.css({ display: 'block' })
						},
						afterClose: function () {
							$('body').removeClass('open-modal')
						},
					},
				})
			}
			if ($signUpButton.length > 0) {
				$signUpButton.on('click', handlerOpenPopup)
			}
		}

		function openSignUpToCourse() {
			var $signUpButton = $('.js-sing-up-of-course')
			var $signUpCourseModal = $('#sign-up-popup')

			function handlerOpenPopup() {
				var button = $(this)
				$.magnificPopup
					.open({
						items: {
							src: '#sign-up-popup',
							type: 'inline',
						},
						fixedContentPos: false,
						fixedBgPos: false,
						closeMarkup: '<button class="mfp-close "></button>',
						callbacks: {
							beforeOpen: function () {
								$('body').addClass('open-modal')
								var courseContent = button.closest('.js-course-details')
								var courseTitle = courseContent.attr('data-course-title')
								$('#sign-up-popup .course-name').val(courseTitle)
								var courseRadioButton = courseContent.find('input:checked')
								if (courseRadioButton) {
									var coursePrice = courseRadioButton.val()
									$('#sign-up-popup .course-price').val(coursePrice)
								}
								$signUpCourseModal.css({ display: 'block' })
							},
							afterClose: function () {
								$('body').removeClass('open-modal')
							},
						},
					})
			}
			if ($signUpButton.length > 0) {
				$signUpButton.on('click', handlerOpenPopup)
			}
		}

		function openSubmitForm() {
			var $submitButton = $('.js-open-submit-form');
			var $submitForm = $('#trave-item-submit-request')

			if ($submitButton.length > 0) {
				$submitButton.on('click', handlerOpenPopup)
			}

			function handlerOpenPopup() {
				var button = $(this)
				$.magnificPopup
					.open({
						items: {
							src: '#trave-item-submit-request',
							type: 'inline',
						},
						fixedContentPos: false,
						fixedBgPos: false,
						closeMarkup: '<button class="mfp-close "></button>',
						callbacks: {
							beforeOpen: function () {
								$('body').addClass('open-modal')
								var courseContent = button.closest('.tour-info')
								var courseTitle = courseContent.attr('data-travel-title')
								$('#course-name').val(courseTitle)
								$submitForm.css({ display: 'block' })
							},
							afterClose: function () {
								$('body').removeClass('open-modal')
							},
						},
					})
			}
		}

		openSignUpToCourse()
		openGiftCourse()
		openSubmitForm()
	})
})
