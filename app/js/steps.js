$(function () {
	$(document).ready(function () {
		function setDropdownList() {
			$(".js-select-post-delivery").dropkick();
			$(".js-select-self-delivery").dropkick();
			$(".js-select-time").dropkick();
		}

		function validationForm(form) {
			$.validator.addMethod(
				"customphone",
				function (value, element) {
					return (
						this.optional(element) ||
						/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(value)
					);
				},
				"Неправильный формат телефона"
			);

			form.validate({
				errorPlacement: function errorPlacement(error, element) {
					if (element.is("input:radio")) {
						element
							.parents(".form__radio_group")
							.find(".form__radio_error")
							.after(error);
					} else {
						element.after(error);
					}
				},
				rules: {
					name: {
						required: true,
					},
					surname: {
						required: true,
					},
					addressSelfDelivery: {
						required: true,
					},
					addressPostDelivery: {
						required: true,
					},
					userInfoSex: {
						required: true,
					},
					postDelivery: {
						required: true,
					},
					selfDelivery: {
						required: true,
					},
					email: {
						required: true,
						email: true,
					},
					phone: {
						customphone: true,
						required: true,
					},
					selectPostAddress: {
						required: true,
					},
					dateRange: {
						required: true,
					},
				},
				messages: {
					name: {
						required: "Поле обязательное для заполнения",
					},
					surname: {
						required: "Поле обязательное для заполнения",
					},
					addressSelfDelivery: {
						required: "Поле обязательное для заполнения",
					},
					addressPostDelivery: {
						required: "Поле обязательное для заполнения",
					},
					phone: {
						required: "Поле обязательное для заполнения",
					},
					email: {
						required: "Поле обязательное для заполнения",
						email: "Неправильный формат",
					},
					postDelivery: {
						required: "Поле обязательное для заполнения",
					},
					selfDelivery: {
						required: "Поле обязательное для заполнения",
					},
					userInfoSex: {
						required: "Поле обязательное для заполнения",
					},
					dateRange: {
						required: "Обязательное поле",
					},
				},
				highlight: function (element, errorClass, validClass) {
					$(element).removeClass("success");
					$(element).addClass("error");
				},
				unhighlight: function (element, errorClass, validClass) {
					$(element).addClass("success");
					$(element).removeClass("error");
				},
			});
		}

		function initSteps(element) {
			if (element && element.length) {
				var form = $(".basket-steps");
				element.steps({
					headerTag: "h3",
					bodyTag: "section",
					transitionEffect: 1,
					transitionEffectSpeed: 500,
					autoFocus: true,
					onStepChanging: function (event, currentIndex, priorIndex) {
						if (currentIndex > priorIndex) {
							return true;
						}

						validationForm(form);
						form.validate().settings.ignore = ":disabled,:hidden";
						return form.valid();
					},
					onStepChanged: function (event, currentIndex, priorIndex) {
						if (currentIndex === 2) {
							initDateRangeBasket();
						}
					},
					onFinishing: function (event, currentIndex) {
						form.validate().settings.ignore =
							":disabled,:hidden:not(.js-select-post-address)";
						return form.valid();
					},
					onFinished: function (event, currentIndex) {
						alert("Submitted!");
					},
				});
			}

			setDropdownList();
		}

		function initDateRangeBasket() {
			var $dateRange = $("#js-basket-datepicker");
			$dateRange.daterangepicker({
				singleDatePicker: true, // r
				autoApply: true,
				autoUpdateInput: false,
				showCustomRangeLabel: false,
				opens: "right", //right
				format: "DD/MM/YYYY",
				locale: {
					cancelLabel: "Clear",
					format: "DD/MM/YYYY",
					daysOfWeek: ["вс", "пн", "вт", "ср", "чт", "пт", "сб"],
					monthNames: [
						"Январь",
						"Февраль",
						"Март",
						"Апрель",
						"Май",
						"Июнь",
						"Июль",
						"Август",
						"Сентябрь",
						"Октябрь",
						"Ноябрь",
						"Декабрь",
					],
					firstDay: 1,
				},
			});
			$dateRange.on("apply.daterangepicker", function (ev, picker) {
				$(this).val(picker.startDate.format("DD-MM-YYYY"));
			});
		}

		function initTabs(contentData) {
			var activeClass = "is-active";
			var tabsElements = $("[data-tabs-name=" + contentData + "]");
			var tabElement = $("[data-tabs-name=" + contentData + "] > li");
			var contentElement = $("[data-content-name = " + contentData + "] > div");
			var contentFirstElement = $(
				"[data-content-name=" + contentData + "] > div:first-child"
			);
			var tabsFirstElement = $(
				"[data-tabs-name=" + contentData + "] > li:first-child"
			);

			function hideElements() {
				console.log("HIDE");
				contentElement.each(function () {
					$(this).css("display", "none");
				});
			}

			hideElements();
			tabsFirstElement.addClass(activeClass);
			contentFirstElement.css("display", "block");

			tabsElements.on("click", "li", function () {
				var index = $(this).index();
				var targetTabName = $(this).parent().attr("data-tabs-name");
				tabElement.each(function () {
					if ($(this).hasClass(activeClass)) {
						$(this).removeClass(activeClass);
					}
				});
				if (!$(this).hasClass(activeClass)) {
					$(this).addClass(activeClass);
				}
				hideElements();
				$("[data-" + targetTabName + '-index="' + index + '"]').css(
					"display",
					"block"
				);
			});
		}

		function setListenerStepsAction(elementNext, elementFinish) {
			if (elementNext.length) {
				elementNext.on("click", function (e) {
					e.preventDefault();
					e.stopPropagation();
					$('a[href="#next"]').trigger("click");
				});
			}

			elementFinish.on("click", function (e) {
				e.preventDefault();
				e.stopPropagation();
				$('a[href="#finish"]').trigger("click");
			});
		}

		var stepsContainer = $(".basket-steps div");
		initSteps(stepsContainer);

		initTabs("delivery");
		initTabs("certificate");

		var stepsButtonAction = $(".js-next-action");
		var finishButtonAction = $(".js-finish-step");
		setListenerStepsAction(stepsButtonAction, finishButtonAction);
	});
});
