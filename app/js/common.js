$(function () {
	$(document).ready(function () {
		var desktopWidth = 992;
		var screenWidth = $(window).width();

		function stickyMenu() {
			var scroll = $(window).scrollTop();
			if (scroll > 0) {
				$(".header").addClass("header__sticky");
			} else {
				$(".header").removeClass("header__sticky");
			}
		}

		function toggleMenu() {
			$("#burger-menu").on("click", function () {
				$(".header").toggleClass("open");
				$("body").toggleClass("menu-open");
			});
		}

		function initCarouselTeamCardHome() {
			var $teamCardHome = $(".root-page .team__cards");
			$teamCardHome.owlCarousel({
				loop: true,
				margin: 30,
				autoWidth: true,
				navText: ["", ""],
				autoWidth: true,
				dots: false,
				responsive: {
					0: {
						items: 1.25,
						nav: false,
						autoWidth: false,
					},
					480: {
						items: 1.75,
						nav: false,
						autoWidth: true,
					},
					768: {
						items: 2.75,
						nav: false,
						autoWidth: false,
					},
					992: {
						items: 4,
						nav: true,
						autoWidth: false,
					},
					1280: {
						items: 4,
						autoWidth: false,
						margin: 60,
						nav: true,
					},
				},
			});
		}

		function initCarouselTourListHome() {
			var $teamCardHome = $(".tours-list .owl-carousel");
			$teamCardHome.owlCarousel({
				loop: false,
				margin: 60,
				nav: true,
				navText: ["", ""],
				dots: false,
				responsive: {
					0: {
						items: 1.25,
						nav: false,
						margin: 20,
					},
					480: {
						items: 1.75,
						nav: false,
						margin: 20,
					},
					600: {
						items: 2.25,
						nav: false,
						margin: 20,
					},
					768: {
						items: 2.75,
						autoWidth: false,
						margin: 20,
					},
					992: {
						items: 3,
						margin: 30,
					},
					1280: {
						items: 3,
					},
				},
			});
		}

		function initCarouselReportListHome() {
			var $reportListHome = $(".reports-list .owl-carousel");

			var owlReport = $reportListHome
				.owlCarousel({
					loop: true,
					responsiveClass: true,
					dots: false,
					margin: 20,
					responsive: {
						0: {
							items: 1.25,
							nav: false,
							autoWidth: false,
						},
						480: {
							items: 2,
							autoWidth: false,
							nav: false,
						},
						550: {
							items: 3,
							autoWidth: false,
							nav: false,
						},
						768: {
							items: 1.5,
							autoWidth: false,
							nav: false,
						},
						992: {
							items: 2,
							autoWidth: false,
							nav: false,
						},
						1150: {
							autoWidth: true,
							items: 3,
						},
						1366: {
							items: 3,
							margin: 40,
						},
					},
				})
				.trigger("refresh.owl.carousel");

			$("#reports-next").click(function () {
				owlReport.trigger("next.owl.carousel");
			});

			$("#reports-prev").click(function () {
				owlReport.trigger("prev.owl.carousel");
			});
		}

		function initCarouselAboutUs() {
			$(".about-description__image .owl-carousel ").owlCarousel({
				loop: true,
				nav: true,
				navText: ["", ""],
				dots: true,
				items: 1,
			});
		}

		function initCarouselCourse() {
			$(window).on("load", function () {
				$(".course__image .owl-carousel ")
					.owlCarousel({
						loop: true,
						nav: true,
						navText: ["", ""],
						dots: true,
						items: 1,
						// autoWidth: true
					})
					.trigger("refresh.owl.carousel");
			});
		}

		function initDropDown() {
			var $defaultDropDown = $(".dropdown__city--simple");
			var $countryDropDown = $(".dropdown__country");
			var $yearDropDown = $(".dropdown__year");
			var $postAddressDropdown = $(".dropdown-post-address");

			function setNiceSelect(el) {
				if (el.length) {
					el.niceSelect();
				}
			}

			setNiceSelect($defaultDropDown);
			setNiceSelect($countryDropDown);
			setNiceSelect($yearDropDown);
			setNiceSelect($postAddressDropdown);
		}

		function initDateRangeBasket() {
			var $dateRange = $("#js-basket-datepicker");
			$dateRange.daterangepicker({
				singleDatePicker: true, // r
				autoApply: true,
				autoUpdateInput: false,
				showCustomRangeLabel: false,
				opens: "right", //right
				format: "DD/MM/YYYY",
				locale: {
					cancelLabel: "Clear",
					format: "DD/MM/YYYY",
					daysOfWeek: ["вс", "пн", "вт", "ср", "чт", "пт", "сб"],
					monthNames: [
						"Январь",
						"Февраль",
						"Март",
						"Апрель",
						"Май",
						"Июнь",
						"Июль",
						"Август",
						"Сентябрь",
						"Октябрь",
						"Ноябрь",
						"Декабрь",
					],
					firstDay: 1,
				},
			});
			$dateRange.on("apply.daterangepicker", function (ev, picker) {
				$(this).val(picker.startDate.format("DD-MM-YYYY"));
			});
		}

		function initDateRange() {
			var $dateRange = $("#txtDateRange");

			function isLinkCalendar() {
				if (screenWidth < 768) {
					return false;
				} else {
					return true;
				}
			}

			if ($dateRange.length) {
				$dateRange.daterangepicker(
					{
						autoApply: true,
						autoUpdateInput: false,
						showCustomRangeLabel: false,
						linkedCalendars: isLinkCalendar(),
						opens: "left", //right
						format: "DD/MM/YYYY",
						locale: {
							cancelLabel: "Clear",
							format: "DD/MM/YYYY",
							daysOfWeek: ["вс", "пн", "вт", "ср", "чт", "пт", "сб"],
							monthNames: [
								"Январь",
								"Февраль",
								"Март",
								"Апрель",
								"Май",
								"Июнь",
								"Июль",
								"Август",
								"Сентябрь",
								"Октябрь",
								"Ноябрь",
								"Декабрь",
							],
							firstDay: 1,
						},
					},
					function (start, end, label) {
						console.log(
							"New date range selected: " +
								start.format("YYYY-MM-DD") +
								" to " +
								end.format("YYYY-MM-DD") +
								" (predefined range: " +
								label +
								")"
						);
					}
				);
				$dateRange.on("apply.daterangepicker", function (ev, picker) {
					$(this).val(
						picker.startDate.format("DD-MM-YYYY") +
							" до " +
							picker.endDate.format("DD-MM-YYYY")
					);
				});
				$dateRange.on("show.daterangepicker", function (ev, picker) {
					picker.linkedCalendars = isLinkCalendar();
					$(".dateRange").addClass("open");
				});
				$dateRange.on("hide.daterangepicker", function () {
					$(".dateRange").removeClass("open");
				});
			}
		}

		function openInstructorCertificateGallery(element) {
			if (element.length) {
				element.magnificPopup({
					type: "image",
					delegate: "a",
					closeOnContentClick: true,
					fixedContentPos: false,
					fixedBgPos: false,
					image: {
						verticalFit: false,
					},
					gallery: {
						enabled: true,
						tCounter: "",
					},
					callbacks: {
						open: function () {
							$("body").addClass("noscroll modal--dark");
						},
						close: function () {
							$("body").removeClass("noscroll modal--dark");
						},
					},
				});
			}
		}

		function openContentImage(listImage) {
			if (listImage.length) {
				listImage.magnificPopup({
					delegate: "a",
					type: "image",
					fixedContentPos: false,
					gallery: {
						enabled: true,
						tCounter: "",
					},
					showCloseBtn: false,
					callbacks: {
						open: function () {
							$("body").addClass("noscroll modal--dark");
						},
						close: function () {
							$("body").removeClass("noscroll modal--dark");
						},
					},
				});
			}
		}

		function initBannerVideoPopup() {
			var bannerVideo = $("#js-banner-video");

			if (bannerVideo.length) {
				bannerVideo.magnificPopup({
					type: "iframe",
					mainClass: "mfp-dark",
					removalDelay: 160,
					preloader: false,
				});
			}
		}

		function initVideo() {
			var video = $(".js-student-video");

			if (video.length) {
				video.magnificPopup({
					type: "iframe",
					mainClass: "mfp-dark",
					removalDelay: 160,
					preloader: false,
					fixedContentPos: false,
					closeMarkup: '<button class="mfp-close"></button>',
					callbacks: {
						open: function () {
							$("body").addClass("modal--dark");
							$("body").css("overflow", "initial");
						},
						close: function () {
							$("body").removeClass("modal--dark");
							$("body").css("overflow-x", "hidden");
						},
					},
				});
			}
		}

		function setSmoothScrolling() {
			$(document).on("click", 'a[href^="#directions"]', function (event) {
				event.preventDefault();

				$("html, body").animate(
					{
						scrollTop: $($.attr(this, "href")).offset().top,
					},
					500
				);
			});
		}

		function initSelectDropdown(element) {
			if (element && element.length) {
				var prevSelected = 0;
				element.dropkick({
					mobile: true,
					open: function () {
						prevSelected = this.selectedIndex;
						this.hide(this.selectedIndex, true);
					},
					change: function () {
						console.log(this);
					},
					close: function () {
						if (this.selectedIndex !== prevSelected) {
							this.hide(prevSelected, false);
							prevSelected = this.selectedIndex;
						}
						this.hide(prevSelected, false);
					},
				});
			}
		}

		function initSelectDropdownPrice(element) {
			if (element && element.length) {
				element.click(function (e) {
					e.preventDefault();
					e.stopPropagation();
					$(this).toggleClass("expanded");
					var target = $(e.target);
					var label = target;
					if (target.is("span")) {
						label = target.parent();
					}
					$("#" + label.attr("for")).prop("checked", true);
				});
				$(document).click(function () {
					element.removeClass("expanded");
				});
			}
		}

		function initOpenModal(element, targetModalElement) {
			if (element.length) {
				element.magnificPopup({
					type: "inline",
					fixedContentPos: false,
					fixedBgPos: false,
					closeMarkup: '<button class="mfp-close "></button>',
					callbacks: {
						beforeOpen: function () {
							$("body").addClass("open-modal");
							targetModalElement.css({ display: "block" });
						},
						afterClose: function () {
							$("body").removeClass("open-modal");
						},
					},
				});
			}
		}

		

		function isPromoExistExtion() {
			$(".modal__action_text ").on("click", function () {
				$(".modal__action_text ").css({ display: "none" });
				$(".modal__action_input").css({ display: "block" });
			});
		}

		function coursesTabs() {
			$(".directions-menu span").on("click", function (e) {
				e.preventDefault();
				setTimeout(function () {
					$(window).resize();
				}, 0);

				if ($(this).siblings(".sub-menu").hasClass("active")) {
					$(this).siblings(".sub-menu").removeClass("active");
				} else {
					$(this).siblings(".sub-menu").addClass("active");
				}

				var countDirs = $(this).parent("li").attr("data-dir");
				if (countDirs) {
					$(".directions-tabs__content").each(function (index) {
						$(this).css({ display: "none" });
					});
					$("[data-id=" + countDirs + "]").css({ display: "block" });
				}
			});
		}

		function coursesTabsHover() {
			$(".sub-menu span").on("click", function (e) {
				e.preventDefault();
				$(".sub-menu span").parent("li").removeClass("active");
				if ($(this).parent("li").hasClass("active")) {
					$(this).parent("li").removeClass("active");
				} else {
					$(this).parent("li").addClass("active");
				}
			});
		}

		function photos() {
			var $secondItem = $('.photos .photos__item:nth-child(2)');
			var $photoItems = $('.photos');
			var $photoItem =$('.photos .photos__item');

			function handleIn() {
				$(this).addClass("hovered");
			}

			function handleOut() {
				$(this).removeClass("hovered");
			}

			function handlerParentOut() {
				$secondItem.addClass("hovered");
			}

			function handleParentIn(e) {
				var targetIndex = $(e.target).parent().index();
				if (targetIndex !== 1) {
					$secondItem.removeClass("hovered");
				}
			}

			$photoItem.hover(handleIn, handleOut);
			$photoItems.mouseleave(handlerParentOut);
			$photoItems.mouseenter(handleParentIn);
		}

		function chooseAnotherCity() {
			var $chooseCityBlock = $('.choose-city');
			var $btnChooseAnotherCity = $('#js-choose-another-city');
			var $confirmCity = $('#js-choose-city-confirm');
			var $dropdownAnotherCity = $('.js-custom-select');
			var $chooseCityOverflow = $('.choose-city-overflow');

			function closeCityBlock() {
				$chooseCityBlock.css('display', 'none');
				$chooseCityOverflow.css('display', 'none');
			}

			function chooseAnotherCityHandler() {
				closeCityBlock();
				setTimeout(function(){
					$dropdownAnotherCity.trigger('click');
				}, 100)
			}

			$confirmCity.on('click', closeCityBlock);
			$btnChooseAnotherCity.on('click', chooseAnotherCityHandler);
			$chooseCityOverflow.on('click', closeCityBlock);

		}

		function handleChangeCity() {
			var $city = $('.js-custom-select');

			$city.on('click', function() {
				$(this).siblings('.custom-selected__list').toggle();
				$(this).toggleClass('open');
			})
		}

		function init() {
			stickyMenu();
			toggleMenu();
			initCarouselTeamCardHome();
			initCarouselTourListHome();
			initCarouselReportListHome();
			initCarouselAboutUs();
			initCarouselCourse();
			initDropDown();
			initDateRange();
			initVideo();
			openContentImage($(".instructor-page .instructor__gallery_list"));
			openContentImage($(".content__image"));
			openContentImage($(".image"));

			var certificateList = $(".js-certificate-list");
			openInstructorCertificateGallery(certificateList);

			setSmoothScrolling();
			initBannerVideoPopup();
			var carousel = $(".directions-tabs__carousel")
				.owlCarousel({
					loop: true,
					nav: true,
					navText: ["", ""],
					margin: 0,
					items: 1,
					dots: true,
					singleItem: true,
					video: true,
					videoWidth: false, // Default false; Type: Boolean/Number
					videoHeight: 500, // Default false; Type: Boolean/Number
				})
				.trigger("refresh.owl.carousel");

			// GLOBAL LISTENER
			$(window).scroll(function () {
				stickyMenu();
			});

			$(window).resize(function () {
				stickyMenu();
				screenWidth = $(window).width();
				carousel.trigger("refresh.owl.carousel");
			});

			var $headerDesktopLanguage = $("#js-header-dropdown-desktop-lang");
			initSelectDropdown($headerDesktopLanguage);

			var $headerDesktopCity = $("#js-header-dropdown-desktop-city");
			initSelectDropdown($headerDesktopCity);

			var $headerMobileLang = $("#js-header-dropdown-mobile-lang");
			initSelectDropdown($headerMobileLang);

			var $headerMobileCity = $("#js-header-dropdown-mobile-city");
			initSelectDropdown($headerMobileCity);

			var $contactCity = $("#js-contact-dropdown-desktop-city");
			initSelectDropdown($contactCity);

			var $dropdownCity = $("#js-dropdown-desktop-city");
			initSelectDropdown($dropdownCity);
			
			var $dropdownCityDirection = $("#js-dropdown-direction");
			initSelectDropdown($dropdownCityDirection);

			$('.js-accept-city-direction').on('click', function(e) {
				console.log($(this).closest('[data-id]'))
			})

			var $directionPrice = $(".direction-dropdown-price");
			initSelectDropdownPrice($directionPrice);

			var modalHeaderCallLink = $("#header-call");
			var modalHeaderCall = $("#modal-header-call");
			initOpenModal(modalHeaderCallLink, modalHeaderCall);

			var modalHeaderMessageLink = $("#header-message");
			var modalHeaderMessage = $("#modal-header-message");
			initOpenModal(modalHeaderMessageLink, modalHeaderMessage);


			isPromoExistExtion();
			coursesTabs();
			coursesTabsHover();
			chooseAnotherCity();
			handleChangeCity()

			if ($('.photos').length) photos();
		}

		init();

		function questionCardInfo() {
			var heightQuestion = $(".card-info").offset().top - 105;

			$(window).scroll(function () {
				var scroll = $(window).scrollTop();

				if (screenWidth > 768) {
					if (scroll >= heightQuestion) {
						questionCard.css({
							position: "fixed",
							top: 100,
							width: "100%",
						});
					} else {
						questionCard.css({
							position: "static",
						});
					}
				} else {
					// console.log("SCREEN MORE")
					questionCard.css({
						position: "static",
					});
				}
			});
		}

		var questionCard = $(".card-info");
		if (questionCard.length) questionCardInfo();

		$(window).resize(function () {
			if (questionCard.length) questionCardInfo();
		});

		// INSTRUCTOR PAGE

		function instructorAvatarPosition() {
			var fixedAvatar = $(".instructor-page .instructor__avatar");
			var bodyHeight = document.body.scrollHeight;
			var paddingHeight = 160;
			var footerHeight = $(".footer").height();
			var h = $(".instructor__avatar").height();
			var a = bodyHeight - footerHeight - h - paddingHeight * 2;

			$(window).scroll(function () {
				var scroll = $(window).scrollTop();
				if (scroll > 0) {
					fixedAvatar.addClass("instructor__avatar--visible");
				} else {
					fixedAvatar.removeClass("instructor__avatar--visible");
				}

				if (scroll <= a) {
					fixedAvatar.css({
						position: "fixed",
						top: 135,
					});
				} else {
					fixedAvatar.css({
						position: "absolute",
						top: a + paddingHeight,
					});
				}
			});
		}

		var instructorPage = $(".instructor-page");

		if (instructorPage.length) instructorAvatarPosition();

		// END INSTRUCTOR PAGE

		function directionsTabs() {
			var $btn = $(".directions-list__item-btn");

			$btn.on("click", function (e) {
				var $parent = $(this).parent();

				$parent.toggleClass("open");
			});
		}
		directionsTabs();
	});
});

